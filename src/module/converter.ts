import mjml2html from 'mjml';
    export class TemplateConverter {

        convert(template: string, properties : [], delimiter: string) : string {

            if(delimiter === undefined) {
                delimiter = "@@@";
            }
            const variables = JSON.parse(JSON.stringify(properties));
            Object.keys(variables).forEach((cleVariable) => {
                template = template.split(delimiter+cleVariable+delimiter).join(variables[cleVariable]);
            });
            if(template.lastIndexOf(delimiter) !== -1) {
                template = template.split(template.substring(template.indexOf(delimiter) + delimiter.length, template.lastIndexOf(delimiter))).join("");
            }
            template = template.split(delimiter).join("");
            return  mjml2html(template, {}).html
        }


}