import {Connection, createConnection} from "typeorm";

export class Singleton {
    private static instance: Singleton;
    private _connexion: Connection;


    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() { }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): Singleton {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton();
        }

        return Singleton.instance;
    }

    /**
     * Finally, any singleton should define some business logic, which can be
     * executed on its instance.
     */
    public initMysql() {

        createConnection().then(async connection => {
            this._connexion = connection;
        }).catch(error => console.log(error));
        return this;
    }


    get connexion(): Connection {
        return this._connexion;
    }
}