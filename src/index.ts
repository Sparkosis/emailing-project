
import {Singleton} from "./core/AppSingleton";
import {TemplateConverter} from "./module/converter";
import fastify from 'fastify';
import {Campagne} from "./entity/Campagne";

let server: fastify.FastifyInstance;
server = fastify({});

Singleton.getInstance().initMysql();



server.register(require('fastify-cors'), {
    origin: true
})
server.register(require('fastify-swagger'), {
    routePrefix: '/documentation',
    swagger: {
        info: {
            title: 'Documentation de l\'api',
            version: '0.1.0'
        },
        tags: [
            { name: 'Convertisseur', description: 'User related end-points' }
        ],
        host: 'localhost:5656',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
    },
    exposeRoute: true
})
const opts = {
    schema: {

        response: {
            200: {
                type: 'object',
                properties: {
                    pong: {
                        type: 'string'
                    }
                }
            }
        },
        tags: ['Convertisseur'],
    }
}
const port = 5656; // default port to listen
server.post('/convert', opts, async (req, reply) => {
    let delimiter : string = "@@@";
    if(req.body.delimiter !== undefined) {
        delimiter = req.body.delimiter;
    }
    let result = "";
    try {
        result = new TemplateConverter().convert(req.body.template, req.body.vars, delimiter)
    } catch (err) {
        console.log(err)
        reply
            .code(400)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Le format MJML de la template envoyée est incorrect')
    }
    return result;
})
server.post('/convert/:id', opts, async (req, reply) => {

    let delimiter : string = "@@@";
    if(req.body.delimiter !== undefined) {
        delimiter = req.body.delimiter;
    }
    let result = "";
    try {
        const idCampagne = req.params.id;
        await
            Singleton.getInstance().connexion.manager.findOne(Campagne, idCampagne).then(value => {
                result = new TemplateConverter().convert(value.template, req.body.vars, delimiter)
            }).catch(reason => {
                reply.callNotFound();
            })

    } catch (err) {
        reply
            .code(400)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Le format MJML de la template envoyée est incorrect')
    }
    return result;
})
server.post('/campagne', opts, async (req, reply) => {
    if(req.body.libelle == undefined || req.body.template == undefined) {
        reply
            .code(400)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Le format de la requête envoyée est incorrecte')
    }
   let campagneToCreate : Campagne = new Campagne();
    campagneToCreate.libelle = req.body.libelle;
    campagneToCreate.template = req.body.template;

    Singleton.getInstance().connexion.manager.save(campagneToCreate).then(result => {
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(JSON.stringify(result))
    }).catch(reason => {
        reply
            .code(500)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(reason)
    })

})
server.delete('/campagne/:id', opts, async (req, reply) => {
   if(req.params.id == undefined) {
       reply
           .code(400)
           .header('Content-Type', 'application/json; charset=utf-8')
           .send('Le format de la requête envoyée est incorrecte, veuillez précider l\'identifiant de la campagne');
   }

    await Singleton.getInstance().connexion.manager.delete(Campagne, req.params.id ).then().catch(reason => {
            reply
                .code(404)
                .header('Content-Type', 'application/json; charset=utf-8')
                .send('Cette campagne n\'existe pas');
        })

    return "deleted";
})
server.get('/campagnes', opts, async (req, reply) => {
    await Singleton.getInstance().connexion.manager.find(Campagne).then(value => {
        reply.code(200).header('Content-Type', 'application/json; charset=utf-8').send(JSON.stringify(value));
    }).catch(reason => {
        reply
            .code(404)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Cette campagne n\'existe pas');
    })
})

server.get('/campagne/:id', opts, async (req, reply) => {
    await Singleton.getInstance().connexion.manager.findOne(Campagne, req.params.id).then(value => {
        reply.code(200).header('Content-Type', 'application/json; charset=utf-8').send(JSON.stringify(value));
    }).catch(reason => {
        reply
            .code(404)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Cette campagne n\'existe pas');
    })
})

server.put('/campagne/:id', opts, async (req, reply) => {
    await Singleton.getInstance().connexion.createQueryBuilder().update(Campagne).set({
        template: req.body.template
    }).where('id = :id', {id: req.params.id}).execute().then(value => {
        reply
            .code(200)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send(JSON.stringify(value));
    }).catch(reason => {
        reply
            .code(404)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send('Cette campagne n\'existe pas');
    })
})
server.listen(port, (err) => {
    if (err) {
        server.log.error(err)
        process.exit(1)
    }
    console.log(`server listening on ${port}`)
})