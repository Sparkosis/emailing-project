import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Campagne {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    libelle: string;

    @Column("text")
    template: string;


}
